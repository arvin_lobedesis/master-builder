<?php 
	$host = "localhost"; //the host that we will use
	$db_username = "root"; //username for host
	$db_password = ""; //password for host
	$db_name = "b55Ecommerce"; //name for database

	// to create connection
	$conn = mysqli_connect($host, $db_username, $db_password, $db_name);

	//to check connection
	if(!$conn){
		die("Connection Failed: " .mysqli_error($conn));
	}
 ?>